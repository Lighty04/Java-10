package hr.fer.zemris.java.gui.prim;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.SwingUtilities;
import javax.swing.WindowConstants;

/**
 * Class which is used as JFrame and is holding two lists with prime numbers and
 * one button. When button is pressed list adds one more prime on those two
 * lists which have same model. When there are too many numbers in list to be
 * shown on screen, then you can scroll all over elements of primes
 * 
 * @author Miroslav Filipovic
 */
public class PrimDemo extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Constructor used to make new window where are all elements which show
	 * prime numbers as many as lists have in their memory
	 */
	public PrimDemo() {
		setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
		setSize(500, 500);
		setLayout(new BorderLayout());

		JPanel centerPanel = new JPanel();
		PrimListModel model = new PrimListModel();
		JList<Integer> jlist1 = new JList<>(model);
		JList<Integer> jlist2 = new JList<>(model);
		JScrollPane scroll1 = new JScrollPane(jlist1);
		JScrollPane scroll2 = new JScrollPane(jlist2);

		JButton button = new JButton("NEXT");

		centerPanel.setLayout(new GridLayout(1, 2));

		centerPanel.add(scroll1, 0, 0);
		centerPanel.add(scroll2, 0, 1);

		add(centerPanel, BorderLayout.CENTER);
		add(button, BorderLayout.SOUTH);

		button.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				model.next();
			}
		});
	}

	/**
	 * Main method, where everything starts. It takes 0 arguments, cannot be any
	 * other number of arguments
	 * 
	 * @param args
	 *            cannot exist
	 */
	public static void main(String[] args) {
		if (args.length != 0) {
			System.err
					.println("Invalid number of arguments. Must have 0 and you have "
							+ args.length);
			System.exit(1);
		}

		SwingUtilities.invokeLater(() -> {
			new PrimDemo().setVisible(true);
		});
	}

}
