package hr.fer.zemris.java.gui.prim;

import java.util.ArrayList;
import java.util.List;

import javax.swing.ListModel;
import javax.swing.event.ListDataEvent;
import javax.swing.event.ListDataListener;

/**
 * Model which is used for JList where we want to more JLists share same model
 * which calculates next prime number and puts it in list which is shown on some
 * window
 * 
 * @author Miroslav Filipovic
 */
public class PrimListModel implements ListModel<Integer> {

	/**
	 * List of prime numbers
	 */
	static private List<Integer> prims = new ArrayList<>();

	/**
	 * List of listeners
	 */
	static private List<ListDataListener> listeners = new ArrayList<>();

	/**
	 * Static block for adding first number in list of primes
	 * */
	static {
		prims.add(new Integer(1));
	}

	@Override
	public int getSize() {
		return prims.size();
	}

	@Override
	public Integer getElementAt(int index) {
		return prims.get(index);
	}

	@Override
	public void addListDataListener(ListDataListener l) {
		listeners = new ArrayList<>(listeners);
		listeners.add(l);
	}

	@Override
	public void removeListDataListener(ListDataListener l) {
		listeners = new ArrayList<>(listeners);
		listeners.remove(l);
	}

	/**
	 * Method which is used to calculate next prime number and put it in list of
	 * our list of primes in this model. It also let all JList with this model
	 * know that something changed in prime list and that they must change it
	 */
	public void next() {
		int last = prims.get(prims.size() - 1);
		while (true) {
			last++;
			if (isPrime(last))
				break;
			;
		}
		prims.add(last);
		ListDataEvent event = new ListDataEvent(this,
				ListDataEvent.INTERVAL_ADDED, prims.size(), prims.size());
		for (ListDataListener l : listeners) {
			l.intervalAdded(event);
		}
	}

	/**
	 * Method used to say if given number is prime number
	 * 
	 * @param n
	 *            number which we want to find out if is prime
	 * @return true if n is prime, otherwise false
	 */
	private boolean isPrime(int n) {
		if (n % 2 == 0)
			return false;
		for (int i = 3; i * i <= n; i += 2) {
			if (n % i == 0)
				return false;
		}
		return true;
	}
}
