package hr.fer.zemris.java.gui.calc;

/**
 * Interface that is used to call primary operators method calculate so they can
 * work their job. We make this interface as strategy, so we can call any
 * operator as we want
 * 
 * @author Miroslav Filipovic
 */
public interface PrimaryOperator {

	/**
	 * Method used to make calculation of two numbers, it depends of class in
	 * which is this method implemented, that calculation will be done
	 * 
	 * @param num1
	 *            first number
	 * @param num2
	 *            second number
	 * @return result of calculation
	 */
	public double calculate(double num1, double num2);
}
