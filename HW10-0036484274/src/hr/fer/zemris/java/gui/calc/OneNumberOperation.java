package hr.fer.zemris.java.gui.calc;

/**
 * Interface that is used to calculate all one number operators as sin, cos, tan
 * and others. It is made as strategy so we can call it on any instance that
 * implements this interface so we can call it any time we want to
 * 
 * @author Miroslav Filipovic
 */
public interface OneNumberOperation {
	
	/**
	 * Method that is used to calculate result over only one given number operations
	 * that doesn't need any other number
	 * 
	 * @param number over which we calculate
	 * @return result of calculation
	 */
	public double calculate(String number);
}
