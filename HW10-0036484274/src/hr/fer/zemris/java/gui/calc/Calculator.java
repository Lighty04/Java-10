package hr.fer.zemris.java.gui.calc;

import hr.fer.zemris.java.gui.calc.UtilOneNumberOperations.ArcCos;
import hr.fer.zemris.java.gui.calc.UtilOneNumberOperations.ArcCtan;
import hr.fer.zemris.java.gui.calc.UtilOneNumberOperations.ArcSin;
import hr.fer.zemris.java.gui.calc.UtilOneNumberOperations.ArcTan;
import hr.fer.zemris.java.gui.calc.UtilOneNumberOperations.Cos;
import hr.fer.zemris.java.gui.calc.UtilOneNumberOperations.Ctan;
import hr.fer.zemris.java.gui.calc.UtilOneNumberOperations.InvLn;
import hr.fer.zemris.java.gui.calc.UtilOneNumberOperations.InvLog;
import hr.fer.zemris.java.gui.calc.UtilOneNumberOperations.InversNumber;
import hr.fer.zemris.java.gui.calc.UtilOneNumberOperations.Ln;
import hr.fer.zemris.java.gui.calc.UtilOneNumberOperations.Log;
import hr.fer.zemris.java.gui.calc.UtilOneNumberOperations.Sin;
import hr.fer.zemris.java.gui.calc.UtilOneNumberOperations.Tan;
import hr.fer.zemris.java.gui.calc.UtilPrimaryOperators.Divide;
import hr.fer.zemris.java.gui.calc.UtilPrimaryOperators.Equal;
import hr.fer.zemris.java.gui.calc.UtilPrimaryOperators.Minus;
import hr.fer.zemris.java.gui.calc.UtilPrimaryOperators.Multiply;
import hr.fer.zemris.java.gui.calc.UtilPrimaryOperators.Plus;
import hr.fer.zemris.java.gui.calc.UtilPrimaryOperators.Power;
import hr.fer.zemris.java.gui.calc.UtilPrimaryOperators.Root;
import hr.fer.zemris.java.gui.layouts.CalcLayout;
import hr.fer.zemris.java.gui.layouts.RCPosition;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;
import javax.swing.WindowConstants;

/**
 * Class which makes calculator with all simple options for calculating over one
 * by one numbers, it takes max two numbers as argument for some actions, or one
 * number. It also has stack on which you can push some elements or pop elements
 * from it if there are any elements. It has inverse checkbox to when user wants
 * inverse of sin, cos, tan and similar operations
 * 
 * @author Miroslav Filipovic
 */
public class Calculator extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8565803969398310L;

	/**
	 * 
	 */
	protected JLabel result;

	/**
	 * All components of calculator
	 */
	protected JComponent[][] components;

	/**
	 * This instance
	 */
	private static Calculator calculator;

	/**
	 * Boolean if last click is operation
	 */
	private boolean lastClickWasOperation = false;

	/**
	 * Saves last result
	 */
	private Double lastResult = 0.0;

	/**
	 * Saves lastOperation
	 */
	private PrimaryOperator lastOperator = null;

	/**
	 * Stack where you can store some number
	 */
	private List<String> stack = new ArrayList<>();

	/**
	 * Constructor used to make calculator over JFrame with all button elements
	 * over CalcLayout over which we give commands and calculate results
	 */
	public Calculator() {
		JPanel panel = new JPanel();
		setContentPane(panel);
		panel.setLayout(new CalcLayout(5));

		String[][] componentNames = getComponentNames();
		components = new JComponent[CalcLayout.COMPONENTS_ROW][CalcLayout.COMPONENTS_COLUMN];

		fillPanel(panel, componentNames, components);

		setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
		setSize(panel.getPreferredSize());
		setMinimumSize(panel.getPreferredSize());

		setNumberActions();

		setOneNumberOperationsActions();

		setPrimaryActions();

		setOtherFuncitons(panel);

		((JCheckBox) components[4][6]).addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if (!((JCheckBox) e.getSource()).isSelected()) {
					removeActionListeners();
					setOneNumberOperationsActions();
					setPowerAction();
					((JButton) components[1][1]).setText("sin");
					((JButton) components[2][1]).setText("cos");
					((JButton) components[3][1]).setText("tan");
					((JButton) components[4][1]).setText("tan");
					((JButton) components[2][0]).setText("log");
					((JButton) components[3][0]).setText("ln");
					((JButton) components[4][0]).setText("x^n");
				} else {
					removeActionListeners();
					setOneNumberOperationsInverseActions();
					setRootAction();
					((JButton) components[1][1]).setText("arcsin");
					((JButton) components[2][1]).setText("arccos");
					((JButton) components[3][1]).setText("arctan");
					((JButton) components[4][1]).setText("arcctan");
					((JButton) components[2][0]).setText("10^");
					((JButton) components[3][0]).setText("e^");
					((JButton) components[4][0]).setText("x^(1/n)");
				}
			}

			private void removeActionListeners() {
				for (ActionListener al : ((JButton) components[1][1])
						.getActionListeners()) {
					((JButton) components[1][1]).removeActionListener(al);
				}
				for (ActionListener al : ((JButton) components[2][1])
						.getActionListeners()) {
					((JButton) components[2][1]).removeActionListener(al);
				}
				for (ActionListener al : ((JButton) components[3][1])
						.getActionListeners()) {
					((JButton) components[3][1]).removeActionListener(al);
				}
				for (ActionListener al : ((JButton) components[4][1])
						.getActionListeners()) {
					((JButton) components[4][1]).removeActionListener(al);
				}
				for (ActionListener al : ((JButton) components[2][0])
						.getActionListeners()) {
					((JButton) components[2][0]).removeActionListener(al);
				}
				for (ActionListener al : ((JButton) components[3][0])
						.getActionListeners()) {
					((JButton) components[3][0]).removeActionListener(al);
				}
				for (ActionListener al : ((JButton) components[4][0])
						.getActionListeners()) {
					((JButton) components[4][0]).removeActionListener(al);
				}
			}
		});
	}

	/**
	 * Sets action for root button operation that takes two arguments which are
	 * numbers
	 */
	private void setRootAction() {
		((JButton) components[4][0]).addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if (lastOperator == null || lastOperator instanceof Equal) {
					lastOperator = new Root();
					lastResult = Double.parseDouble(result.getText().trim());
				} else {
					lastResult = lastOperator.calculate(lastResult,
							Double.parseDouble(result.getText().trim()));
					lastOperator = (lastOperator instanceof Equal ? null
							: new Root());
					result.setText(lastResult + " ");
				}
				lastClickWasOperation = true;
			}
		});
	}

	/**
	 * Sets actions of inverse one number operations as arcsin, arccos, arctan
	 * and others which need only one number to calucate result
	 */
	private void setOneNumberOperationsInverseActions() {

		OneNumberOperation[][] oneNumberOperations = new OneNumberOperation[][] {
				{ new InversNumber(), new ArcSin() },
				{ new InvLog(), new ArcCos() }, { new InvLn(), new ArcTan() },
				{ null, new ArcCtan() } };

		for (int i = 1; i < 5; i++) {
			for (int j = 0; j < 2; j++) {

				final int row = i;
				final int column = j;
				if (oneNumberOperations[i - 1][j] == null)
					continue;

				((JButton) components[i][j])
						.addActionListener(new ActionListener() {
							@Override
							public void actionPerformed(ActionEvent e) {
								double num = oneNumberOperations[row - 1][column]
										.calculate(result.getText());
								result.setText(num + " ");
								lastClickWasOperation = true;
								lastResult = Double.parseDouble(result
										.getText());
							}
						});
			}
		}
	}

	/**
	 * Setting actions for buttons that are no logically connected to other
	 * buttons
	 * 
	 * @param panel
	 *            where we set all other actions
	 */
	private void setOtherFuncitons(JPanel panel) {

		((JButton) components[0][6]).addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				result.setText("0 ");
			}
		});

		((JButton) components[1][6]).addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				result.setText("0 ");
				lastClickWasOperation = false;
				lastResult = 0.0;
				lastOperator = null;
			}
		});

		((JButton) components[2][6]).addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				stack.add(result.getText());
				result.setText("0 ");
			}
		});

		((JButton) components[3][6]).addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				if (stack.isEmpty()) {
					JOptionPane.showMessageDialog(panel, "Stack is empty");
				} else {
					result.setText(stack.remove(stack.size() - 1));
				}
			}
		});

	}

	/**
	 * Sets actions for all primary operators as plus minus multiply and others
	 * which takes two numbers as arguments
	 */
	private void setPrimaryActions() {

		PrimaryOperator[] operations = new PrimaryOperator[] { new Equal(),
				new Divide(), new Multiply(), new Minus(), new Plus(),
				new Power() };

		for (int i = 0; i < CalcLayout.COMPONENTS_ROW; i++) {
			final int row = i;
			((JButton) components[i][5])
					.addActionListener(new ActionListener() {
						@Override
						public void actionPerformed(ActionEvent e) {
							if (lastOperator == null
									|| lastOperator instanceof Equal) {
								lastOperator = operations[row];
								lastResult = Double.parseDouble(result
										.getText().trim());
							} else {
								lastResult = lastOperator.calculate(lastResult,
										Double.parseDouble(result.getText()
												.trim()));
								lastOperator = (lastOperator instanceof Equal ? null
										: operations[row]);
								result.setText(lastResult + " ");
							}
							lastClickWasOperation = true;
						}
					});
		}

		setPowerAction();
	}

	/**
	 * Sets action for button power on n-th potention, takes two arguments
	 */
	private void setPowerAction() {
		((JButton) components[4][0]).addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if (lastOperator == null || lastOperator instanceof Equal) {
					lastOperator = new Power();
					lastResult = Double.parseDouble(result.getText().trim());
				} else {
					lastResult = lastOperator.calculate(lastResult,
							Double.parseDouble(result.getText().trim()));
					lastOperator = (lastOperator instanceof Equal ? null
							: new Power());
					result.setText(lastResult + " ");
				}
				lastClickWasOperation = true;
			}
		});
	}

	/**
	 * Sets actions for all one number operators that calculates resutl over
	 * only one number and gives back result
	 */
	private void setOneNumberOperationsActions() {

		OneNumberOperation[][] oneNumberOperations = new OneNumberOperation[][] {
				{ new InversNumber(), new Sin() }, { new Log(), new Cos() },
				{ new Ln(), new Tan() }, { null, new Ctan() } };

		for (int i = 1; i < 5; i++) {
			for (int j = 0; j < 2; j++) {

				final int row = i;
				final int column = j;
				if (oneNumberOperations[i - 1][j] == null)
					continue;

				((JButton) components[i][j])
						.addActionListener(new ActionListener() {
							@Override
							public void actionPerformed(ActionEvent e) {
								double num = oneNumberOperations[row - 1][column]
										.calculate(result.getText());
								result.setText(num + " ");
								lastClickWasOperation = true;
								lastResult = Double.parseDouble(result
										.getText());
							}
						});
			}
		}

	}

	/**
	 * Method which sets actions for numbers on calculator, when you press some
	 * button with number than JLabel adds one more number
	 */
	private void setNumberActions() {
		for (int i = 1; i < 5; i++) {
			for (int j = 2; j < 5; j++) {

				final int row = i;
				final int column = j;
				((JButton) components[i][j])
						.addActionListener(new ActionListener() {

							@Override
							public void actionPerformed(ActionEvent e) {
								if (lastClickWasOperation == true)
									result.setText("0 ");
								String s = result.getText().trim()
										+ ((JButton) e.getSource()).getText();
								// if for +/- button, other buttons 0-9 and .
								// are same
								if (row == 4 && column == 3) {
									s = result.getText();
									if (s.startsWith("-")) {
										s = s.replaceFirst("-", "");
									} else {
										s = "-" + s;
									}
									result.setText(s);
									return;
								}
								try {
									Double.parseDouble(s);
								} catch (Exception e2) {
									return;
								}
								if (s.trim().startsWith("0.")) {
									result.setText(s);
								} else if (s.matches("[0]*")) {
									result.setText("0 ");
								} else {
									result.setText(s.replaceFirst("[0]*", ""));
								}
								lastClickWasOperation = false;
							}
						});
			}
		}
	}

	/**
	 * Method used to fill panel with all elements, one label, one checkbox and
	 * all other are buttons
	 * 
	 * @param panel
	 *            which we fill
	 * @param componentNames
	 *            name of all components
	 * @param components
	 *            that we put in panel
	 */
	private void fillPanel(JPanel panel, String[][] componentNames,
			JComponent[][] components) {
		JLabel label = new JLabel("0 ");
		label.setHorizontalAlignment(JTextField.RIGHT);
		label.setOpaque(true);
		label.setBackground(Color.YELLOW);
		label.setBorder(BorderFactory.createLineBorder(Color.BLUE, 1));
		result = label;

		components[0][0] = label;
		components[4][6] = new JCheckBox(componentNames[4][6]);
		panel.add(components[0][0], new RCPosition(1, 1));
		panel.add(components[4][6], new RCPosition(5, 7));

		for (int i = 0; i < CalcLayout.COMPONENTS_ROW; i++) {
			for (int j = 0; j < CalcLayout.COMPONENTS_COLUMN; j++) {
				if (componentNames[i][j] == null)
					continue;
				if ((i == 0 && j == 0) || (i == 4 && j == 6))
					continue;
				components[i][j] = new JButton(componentNames[i][j]);
				panel.add(components[i][j], new RCPosition(i + 1, j + 1));
			}
		}

	}

	/**
	 * Method used to get names of all button names as string[][]
	 * 
	 * @return string array with all names of buttons
	 */
	private String[][] getComponentNames() {
		return new String[][] { { "0 ", null, null, null, null, "=", "clr" },
				{ "1/x", "sin", "7", "8", "9", "/", "res" },
				{ "log", "cos", "4", "5", "6", "*", "push" },
				{ "ln", "tan", "1", "2", "3", "-", "pop" },
				{ "x^n", "ctg", "0", "+/-", ".", "+", "Inv" } };
	}

	/**
	 * Main where everything starts
	 * 
	 * @param args
	 *            are not used here
	 */
	public static void main(String[] args) {
		SwingUtilities.invokeLater(() -> {
			calculator = new Calculator();
			calculator.setVisible(true);
		});
	}
}
