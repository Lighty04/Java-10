package hr.fer.zemris.java.gui.calc;

/**
 * Class that contains many little static classes that represent operations on
 * calculator. They all implement PrimaryOperator and have just one method which
 * when is called does work that that operator should do
 * 
 * @author Miroslav Filipovic
 */
public class UtilPrimaryOperators {

	/**
	 * Operator equal
	 * 
	 * @author Miroslav Filipovic
	 */
	public static class Equal implements PrimaryOperator {
		@Override
		public double calculate(double num1, double num2) {
			// doesn't matter what is the value
			return 0.0;
		}
	}

	/**
	 * Operator divide
	 * 
	 * @author Miroslav Filipovic
	 */
	public static class Divide implements PrimaryOperator {
		@Override
		public double calculate(double num1, double num2) {
			return num1 / num2;
		}
	}

	/**
	 * Operatpr multiply
	 * 
	 * @author Miroslav Filipovic
	 */
	public static class Multiply implements PrimaryOperator {
		@Override
		public double calculate(double num1, double num2) {
			return num1 * num2;
		}
	}

	/**
	 * Operator minus
	 * 
	 * @author Miroslav Filipovic
	 */
	public static class Minus implements PrimaryOperator {
		@Override
		public double calculate(double num1, double num2) {
			return num1 - num2;
		}
	}

	/**
	 * Operator plus
	 * 
	 * @author Miroslav Filipovic
	 */
	public static class Plus implements PrimaryOperator {
		@Override
		public double calculate(double num1, double num2) {
			return num1 + num2;
		}
	}

	/**
	 * Operator power
	 * 
	 * @author Miroslav Filipovic
	 */
	public static class Power implements PrimaryOperator {
		@Override
		public double calculate(double num1, double num2) {
			return Math.pow(num1, num2);
		}
	}

	/**
	 * Operator n-th root
	 * 
	 * @author Miroslav Filipovic
	 */
	public static class Root implements PrimaryOperator {
		@Override
		public double calculate(double num1, double num2) {
			return Math.pow(num1, 1.0 / num2);
		}
	}

}
