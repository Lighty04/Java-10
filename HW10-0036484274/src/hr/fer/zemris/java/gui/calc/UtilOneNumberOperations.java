package hr.fer.zemris.java.gui.calc;

/**
 * Class that has more little classes that have operators that only need one
 * number to make calculation as sinus, cosinus, tangens and other. They all
 * implement OneNumberOperator as strategy so we can call any method as we like
 * to over OneNumberOperator instance
 * 
 * @author Miroslav Filipovic
 */
public class UtilOneNumberOperations {

	/**
	 * Class which calculates sin
	 * 
	 * @author Miroslav Filipovic
	 */
	static public class Sin implements OneNumberOperation {
		@Override
		public double calculate(String number) {
			double num = Double.parseDouble(number);
			return Math.sin(num);
		}
	}

	/**
	 * Class which calculates cos
	 * 
	 * @author Miroslav Filipovic
	 */
	static public class Cos implements OneNumberOperation {
		@Override
		public double calculate(String number) {
			double num = Double.parseDouble(number);
			return Math.cos(num);
		}
	}

	/**
	 * Class which calculates tan
	 * 
	 * @author Miroslav Filipovic
	 */
	static public class Tan implements OneNumberOperation {
		@Override
		public double calculate(String number) {
			double num = Double.parseDouble(number);
			return Math.tan(num);
		}
	}

	/**
	 * Class which calculates ctan
	 * 
	 * @author Miroslav Filipovic
	 */
	static public class Ctan implements OneNumberOperation {
		@Override
		public double calculate(String number) {
			double num = Double.parseDouble(number);
			return 1.0 / Math.tan(num);
		}
	}

	/**
	 * Class which calculates 1/number
	 * 
	 * @author Miroslav Filipovic
	 */
	static public class InversNumber implements OneNumberOperation {
		@Override
		public double calculate(String number) {
			double num = Double.parseDouble(number);
			return 1.0 / num;
		}
	}

	/**
	 * Class which calculate log
	 * 
	 * @author Miroslav Filipovic
	 */
	static public class Log implements OneNumberOperation {
		@Override
		public double calculate(String number) {
			double num = Double.parseDouble(number);
			return Math.log10(num);
		}
	}

	/**
	 * Class which calculate loge
	 * 
	 * @author Miroslav Filipovic
	 */
	static public class Ln implements OneNumberOperation {
		@Override
		public double calculate(String number) {
			double num = Double.parseDouble(number);
			return Math.log(num);
		}
	}

	/**
	 * Class which calculate arcsin
	 * 
	 * @author Miroslav Filipovic
	 */
	static public class ArcSin implements OneNumberOperation {
		@Override
		public double calculate(String number) {
			double num = Double.parseDouble(number);
			return Math.asin(num);
		}
	}

	/**
	 * Class which calculate arccos
	 * 
	 * @author Miroslav Filipovic
	 */
	static public class ArcCos implements OneNumberOperation {
		@Override
		public double calculate(String number) {
			double num = Double.parseDouble(number);
			return Math.acos(num);
		}
	}

	/**
	 * Class which calculate arctan
	 * 
	 * @author Miroslav Filipovic
	 */
	static public class ArcTan implements OneNumberOperation {
		@Override
		public double calculate(String number) {
			double num = Double.parseDouble(number);
			return Math.atan(num);
		}
	}

	/**
	 * Class which calculate arcctan
	 * 
	 * @author Miroslav Filipovic
	 */
	static public class ArcCtan implements OneNumberOperation {
		@Override
		public double calculate(String number) {
			double num = Double.parseDouble(number);
			return Math.atan(1 / num);
		}
	}

	/**
	 * Class which calculate 10^number
	 * 
	 * @author Miroslav Filipovic
	 */
	static public class InvLog implements OneNumberOperation {
		@Override
		public double calculate(String number) {
			double num = Double.parseDouble(number);
			return Math.pow(10, num);
		}
	}

	/**
	 * Class which calculate e^number
	 * 
	 * @author Miroslav Filipovic
	 */
	static public class InvLn implements OneNumberOperation {
		@Override
		public double calculate(String number) {
			double num = Double.parseDouble(number);
			return Math.pow(Math.E, num);
		}
	}
}
