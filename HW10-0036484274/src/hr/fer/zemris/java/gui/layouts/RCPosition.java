package hr.fer.zemris.java.gui.layouts;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Class used for putting components into CalcLayout over this class, which has
 * row and column on which our component should end. If numbers are inavlid, it
 * will throw exception
 * 
 * @author Miroslav Filipovic
 */
public class RCPosition {

	/**
	 * Row of element
	 * */
	private int row;

	/**
	 * Column of element
	 */
	private int column;

	/**
	 * Constructor which takes row and column of component
	 * 
	 * @param row
	 *            of component
	 * @param column
	 *            of component
	 */
	public RCPosition(int row, int column) {
		super();
		this.row = row;
		this.column = column;

		if (row < 1 || row > 5 || column < 1 || column > 7
				|| (row == 1 && (column < 6 && column > 1))) {
			throw new IllegalArgumentException(
					"Input for RCPostion is wrong, your input was " + row + ","
							+ column);
		}
	}

	/**
	 * Constructor which takes string which must be as "number , number" to make
	 * this instance with given row and column over string
	 * 
	 * @param rowAndColumn
	 *            row and column of component
	 */
	public RCPosition(String rowAndColumn) {
		rowAndColumn = rowAndColumn.replaceAll("\\s+", "");

		String p = "(\\d),(\\d)";
		Pattern pattern = Pattern.compile(p);
		Matcher matcher = pattern.matcher(rowAndColumn);

		if (matcher.find()) {
			row = Integer.parseInt(matcher.group(1));
			column = Integer.parseInt(matcher.group(2));
		} else {
			throw new IllegalArgumentException(
					"Input for RCPostion is wrong, your input was "
							+ rowAndColumn);
		}
		if (row < 1 || row > 5 || column < 1 || column > 7
				|| (row == 1 && (column < 6 && column > 1))) {
			throw new IllegalArgumentException(
					"Input for RCPostion is wrong, your input was "
							+ rowAndColumn);
		}
	}

	/**
	 * Getter for row
	 * 
	 * @return row
	 */
	public int getRow() {
		return row;
	}

	/**
	 * Getter for column
	 * 
	 * @return column
	 */
	public int getColumn() {
		return column;
	}

}
