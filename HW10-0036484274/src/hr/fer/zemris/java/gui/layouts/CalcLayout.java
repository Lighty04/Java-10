package hr.fer.zemris.java.gui.layouts;

import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.LayoutManager2;

/**
 * Class which represent calculator layout, has 32 spaces for elements, and
 * cannot have more then that. It will make minimal size layout for given
 * components.
 * 
 * @author Miroslav Filipovic
 */
public class CalcLayout implements LayoutManager2 {

	/**
	 * Number of spaces for component 0,0
	 */
	private static final int NUMBER_OF_UNUSED_FIRST_ROW = 5;

	/**
	 * Start width
	 */
	private static final int START_WIDTH = 10;

	/**
	 * Start height
	 */
	private static final int START_HEIGHT = 40;

	/**
	 * Number of rows
	 */
	public static final int COMPONENTS_ROW = 5;

	/**
	 * Numbero of columns
	 */
	public static final int COMPONENTS_COLUMN = 7;

	/**
	 * Gaps between components
	 */
	private int gap;

	/**
	 * Max height of layout
	 */
	private double maxHeight = 0;

	/**
	 * Max width of layout
	 */
	private double maxWidth = 0;

	/**
	 * Components of layout
	 */
	private Component[][] components = new Component[COMPONENTS_ROW][COMPONENTS_COLUMN];

	/**
	 * Layout width
	 */
	private int layoutWidth = 0;

	/**
	 * Layout height
	 */
	private int layoutHeight = 0;

	/**
	 * Default contructor
	 * */
	public CalcLayout() {
		this(0);
	}

	/**
	 * Constructor which takes gaps between two components in layout
	 * 
	 * @param gap
	 *            between components
	 */
	public CalcLayout(int gap) {
		this.gap = gap;
	}

	@Override
	public void addLayoutComponent(String name, Component comp) {
		// ignore
	}

	@Override
	public void removeLayoutComponent(Component comp) {
		for (int i = 0; i < COMPONENTS_ROW; i++) {
			for (int j = 0; j < COMPONENTS_COLUMN; j++) {
				if (components[i][j].equals(comp)) {
					components[i][j] = null;
				}
			}
		}
	}

	@Override
	public Dimension preferredLayoutSize(Container parent) {
		return new Dimension(layoutWidth, layoutHeight);
	}

	@Override
	public Dimension minimumLayoutSize(Container parent) {
		int minWidth = Integer.MAX_VALUE;
		int minHeight = Integer.MAX_VALUE;
		for (int i = 1; i <= COMPONENTS_ROW; i++) {
			for (int j = 1; j <= COMPONENTS_COLUMN; j++) {
				if (components[i - 1][j - 1] == null || (i == 1 && j == 1))
					continue;
				if (minWidth > components[i - 1][j - 1].getPreferredSize()
						.getWidth())
					minWidth = (int) components[i - 1][j - 1]
							.getPreferredSize().getWidth();
				if (minHeight > components[i - 1][j - 1].getPreferredSize()
						.getHeight())
					minHeight = (int) components[i - 1][j - 1]
							.getPreferredSize().getHeight();

			}
		}
		return new Dimension(minWidth, minHeight);
	}

	@Override
	public void layoutContainer(Container parent) {
		Dimension panelDim = parent.getSize();
		int componentWidth = (panelDim.width - 8 * gap) / COMPONENTS_COLUMN;
		int componentHeight = (panelDim.height - 6 * gap) / COMPONENTS_ROW;
		maxWidth = componentWidth;
		maxHeight = componentHeight;
		for (int i = 1; i <= COMPONENTS_ROW; i++) {
			for (int j = 1; j <= COMPONENTS_COLUMN; j++) {
				if (components[i - 1][j - 1] == null)
					continue;
				int height = (int) ((i * maxHeight) - maxHeight);
				int width = (int) ((j * maxWidth) - maxWidth);
				if (i == 1 && j == 1) {
					components[i - 1][j - 1].setBounds((width + gap), height
							+ gap, NUMBER_OF_UNUSED_FIRST_ROW
							* (int) (componentWidth + gap) - gap,
							(int) componentHeight);
				} else {
					components[i - 1][j - 1].setBounds(width + j * gap, height
							+ i * gap, (int) componentWidth,
							(int) componentHeight);
				}
				components[i - 1][j - 1]
						.setFont(new Font("Serif", Font.PLAIN,
								(int) (components[i - 1][j - 1].getSize()
										.getHeight() * 1 / 2)));
			}
		}
	}

	@Override
	public void addLayoutComponent(Component comp, Object constraints) {
		boolean flag = false;
		if (comp.getPreferredSize() != null
				&& maxHeight < comp.getPreferredSize().getHeight()) {
			maxHeight = comp.getPreferredSize().getHeight();
			flag = true;
		}
		if (comp.getPreferredSize() != null
				&& maxWidth < comp.getPreferredSize().getWidth()) {
			maxWidth = comp.getPreferredSize().getWidth();
			flag = true;
		}
		if (flag) {
			repairBounds();
		}

		RCPosition rcpos = (RCPosition) constraints;
		int row = rcpos.getRow();
		int column = rcpos.getColumn();
		int height = (int) ((row * maxHeight) - maxHeight);
		int width = (int) ((column * maxWidth) - maxWidth);

		if (rcpos.getRow() == 1 && rcpos.getColumn() == 1) {
			comp.setBounds((width + gap), height + gap,
					NUMBER_OF_UNUSED_FIRST_ROW * ((int) maxWidth + gap) - gap,
					(int) maxHeight);
		} else {
			comp.setBounds(width + column * gap, height + row * gap,
					(int) maxWidth, (int) maxHeight);
		}

		layoutWidth = (int) (7 * maxWidth) + 8 * gap + START_WIDTH;
		layoutHeight = (int) (5 * maxHeight) + 6 * gap + START_HEIGHT;
		if (components[row - 1][column - 1] != null)
			throw new IllegalArgumentException(
					"You cannot make two components on same place (" + row
							+ "," + column + ")");
		components[row - 1][column - 1] = comp;
	}

	/**
	 * Method used to repair bounds which is used when some component is added
	 * to layout which needs more space in layout so we must private attributes
	 * repair to make space for all components
	 */
	private void repairBounds() {
		for (int i = 1; i <= COMPONENTS_ROW; i++) {
			for (int j = 1; j <= COMPONENTS_COLUMN; j++) {
				if (components[i - 1][j - 1] == null)
					continue;
				int height = (int) ((i * maxHeight) - maxHeight);
				int width = (int) ((j * maxWidth) - maxWidth);
				if (i == 1 && j == 1) {
					components[i - 1][j - 1].setBounds((width + gap), height
							+ gap, NUMBER_OF_UNUSED_FIRST_ROW
							* (int) (maxWidth + gap) - gap, (int) maxHeight);
				} else {
					components[i - 1][j - 1].setBounds(width + j * gap, height
							+ i * gap, (int) maxWidth, (int) maxHeight);
				}
			}
		}
	}

	@Override
	public Dimension maximumLayoutSize(Container target) {
		return new Dimension(Integer.MAX_VALUE, Integer.MAX_VALUE);
	}

	@Override
	public float getLayoutAlignmentX(Container target) {
		return 0.5f;
	}

	@Override
	public float getLayoutAlignmentY(Container target) {
		return 0.5f;
	}

	@Override
	public void invalidateLayout(Container target) {
		// ignore
	}
}
