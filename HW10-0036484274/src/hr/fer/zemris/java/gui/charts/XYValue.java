package hr.fer.zemris.java.gui.charts;

/**
 * Class that is used to store x and y value for charts so we know on chart
 * where is one rectangle positioned and how big is it
 * 
 * @author Miroslav Filipovic
 */
public class XYValue implements Comparable<XYValue> {

	/**
	 * X coordinate
	 */
	private int x;

	/**
	 * Y coordinate
	 */
	private int y;

	/**
	 * Constructor used to make new instance of this classs with x and y params
	 * 
	 * @param x
	 *            coordinate
	 * @param y
	 *            coordinate
	 */
	public XYValue(int x, int y) {
		super();
		this.x = x;
		this.y = y;
	}

	/**
	 * Getter for x
	 * 
	 * @return x
	 */
	public int getX() {
		return x;
	}

	/**
	 * Getter for y
	 * 
	 * @return y
	 */
	public int getY() {
		return y;
	}

	@Override
	public int compareTo(XYValue o) {
		return this.x - o.x;
	}

}
