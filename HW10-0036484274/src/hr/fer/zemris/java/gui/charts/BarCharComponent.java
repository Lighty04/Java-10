package hr.fer.zemris.java.gui.charts;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.geom.AffineTransform;
import java.util.List;

import javax.swing.JComponent;

/**
 * Class made to make new component which has bars painted on some windows. It
 * makes bars out of informations from BarChart and draws it on some frame.
 * 
 * @author Miroslav Filipovic
 */
public class BarCharComponent extends JComponent {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	/**
	 * BarChart from which we take informations
	 */
	private BarChart barChart;

	/**
	 * Constructor used to get bar chart to take its informations when making component
	 * 
	 * @param barChart from which we take informations
	 */
	public BarCharComponent(BarChart barChart) {
		super();
		this.barChart = barChart;
	}

	@Override
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		int numberOfRectangles = this.barChart.getList().size();
		int maxHeight = this.barChart.getyMax();
		int minHeight = this.barChart.getyMin();
		List<XYValue> list = this.barChart.getList();
		int gap = this.barChart.getGap();
		int screenWidth = this.getParent().getWidth();
		int screenHeight = this.getParent().getHeight();
		int scale = 11;
		int spaceforystring = 20;
		int infospaceleft = (int) (Math.log10(maxHeight) * 20 + spaceforystring);
		int infospacedown = 40;
		int spacefromMaxHeigth = (int) (0.05 * screenHeight);
		int StringHeight = 10;

		g.setColor(Color.GREEN);

		for (int i = 0; i < numberOfRectangles; i++) {
			int width = (screenWidth - infospaceleft) / numberOfRectangles;
			int screenHeightHelp = screenHeight - infospacedown
					- spacefromMaxHeigth;
			g.setColor(Color.GREEN);
			g.fill3DRect(
					infospaceleft + i * width,
					(int) (-infospacedown + screenHeightHelp - screenHeightHelp
							* (list.get(i).getY() * 1.0 / maxHeight))
							+ infospacedown + spacefromMaxHeigth,
					width - gap,
					(int) ((screenHeightHelp * (list.get(i).getY() * 1.0 / maxHeight))),
					true);
			g.drawLine(infospaceleft + i * width, 0, infospaceleft + (i)
					* width, screenHeight - infospacedown
					+ (int) (infospacedown * 0.1));
			g.setColor(Color.BLACK);
			g.drawString("" + list.get(i).getX(), (infospaceleft + i * width
					+ infospaceleft + (i + 1) * width)
					/ 2 - (int) Math.log10(list.get(i).getX()) * 3 / 2,
					screenHeight - infospacedown + StringHeight);
		}

		g.setColor(Color.BLACK);
		int stringXLength = this.barChart.getxString().length();
		int stringWidth = 3;
		g.drawString(this.barChart.getxString(), infospaceleft
				+ (screenWidth - infospaceleft) / 2 - stringXLength
				* stringWidth, screenHeight - infospacedown / 3);
		g.drawLine(infospaceleft, 0, infospaceleft, (int) (screenHeight
				- infospacedown + infospacedown * 0.1));
		g.drawLine((int) (infospaceleft - infospaceleft * 0.1), -infospacedown
				+ screenHeight - 1, screenWidth, -infospacedown + screenHeight
				- 1);

		for (int i = 0; i <= scale; i++) {
			g.setColor(Color.GREEN);
			int screenHeightHelp = screenHeight - infospacedown
					- spacefromMaxHeigth;
			if (i != 0) {
				g.drawLine((int) (infospaceleft - infospaceleft * 0.1),
						spacefromMaxHeigth - 1 + screenHeightHelp
								- (i * screenHeightHelp / scale), screenWidth,
						spacefromMaxHeigth - 1 + screenHeightHelp
								- (i * screenHeightHelp / scale));
			}
			g.setColor(Color.BLACK);
			g.drawString((minHeight + i * (maxHeight - minHeight) / (scale))
					+ "", (int) (infospaceleft * 0.65), spacefromMaxHeigth - 1
					+ screenHeightHelp - i * (screenHeightHelp / scale));
		}

		g.drawString(">", screenWidth - 6, screenHeight - infospacedown + 4);

		AffineTransform at = new AffineTransform();
		at.rotate(-Math.PI / 2);

		Graphics2D g2d = (Graphics2D) g;
		g2d.setTransform(at);

		int yStringLength = this.barChart.getyString().length() * 3 / 2;
		g2d.drawString(this.barChart.getyString(), -screenHeight / 2
				- yStringLength, infospaceleft / 2);
		g.drawString(">", -22, infospaceleft + 5);

		at.rotate(Math.PI / 2);

	}
}
