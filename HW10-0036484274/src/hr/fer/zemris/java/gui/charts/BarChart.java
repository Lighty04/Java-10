package hr.fer.zemris.java.gui.charts;

import java.util.Collections;
import java.util.List;

/**
 * Class used to have all informations about bar chart that we will draw over
 * our BarCharComponent which takes all informations from this instance
 * 
 * @author Miroslav Filipovic
 * */
public class BarChart {

	/**
	 * List of all bars in chart
	 */
	private List<XYValue> list;

	/**
	 * String written on x
	 */
	private String xString;

	/**
	 * String written on y
	 */
	private String yString;

	/**
	 * Maximal y value
	 */
	private int yMax;

	/**
	 * Minimal y value
	 */
	private int yMin;

	/**
	 * Gap between bars
	 */
	private int gap;

	/**
	 * Constructor for making new bar chart instance
	 * 
	 * @param list
	 *            of all bars
	 * @param xString
	 *            string on x
	 * @param yString
	 *            string on y
	 * @param yMin
	 *            minimal y
	 * @param yMax
	 *            maximal y
	 * @param gap
	 *            gap between bars
	 */
	public BarChart(List<XYValue> list, String xString, String yString,
			int yMin, int yMax, int gap) {
		super();
		this.list = list;
		Collections.sort(this.list);
		this.xString = xString;
		this.yString = yString;
		this.yMin = yMin;
		this.yMax = yMax;
		this.gap = gap;
	}

	/**
	 * Getter for all elements
	 * 
	 * @return list of elements
	 */
	public List<XYValue> getList() {
		return list;
	}

	/**
	 * Getter for string on x
	 * 
	 * @return xString
	 */
	public String getxString() {
		return xString;
	}

	/**
	 * Getter for string on y
	 * 
	 * @return yString
	 */
	public String getyString() {
		return yString;
	}

	/**
	 * Getter for maximal y
	 * 
	 * @return yMax
	 */
	public int getyMax() {
		return yMax;
	}

	/**
	 * Getter for minimal y
	 * 
	 * @return yMin
	 */
	public int getyMin() {
		return yMin;
	}

	/**
	 * Getter for gap
	 * 
	 * @return gap
	 */
	public int getGap() {
		return gap;
	}

}
