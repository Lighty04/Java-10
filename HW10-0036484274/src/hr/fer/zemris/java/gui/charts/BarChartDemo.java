package hr.fer.zemris.java.gui.charts;

import java.awt.BorderLayout;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;
import javax.swing.WindowConstants;

/**
 * Class that is used to demonstrate how our BarCharComponent works in frame. It
 * takes file that contains data over arguments, it takes only one argument, if
 * more or less are given program throws exception.
 * 
 * @author Miroslav Filipovic
 */
public class BarChartDemo extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * List of bars
	 */
	static private List<XYValue> list = new ArrayList<>();

	/**
	 * String on x
	 */
	static private String stringX;

	/**
	 * String on y
	 */
	static private String stringY;

	/**
	 * Minimal y
	 */
	static private int minY;

	/**
	 * Maximal y
	 */
	static private int maxY;

	/**
	 * Gap between bars
	 */
	static private int gap;

	/**
	 * Name of file
	 */
	static private String fileName;

	/**
	 * Constructor for making new instance of frame that draws bar chart with
	 * its components, and at top of screen it is written from which file we
	 * took data
	 */
	public BarChartDemo() {
		setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
		setSize(500, 500);

		setLayout(new BorderLayout());
		JPanel panel = new JPanel();

		panel.setLayout(new BorderLayout());
		add(panel, BorderLayout.CENTER);
		JLabel label = new JLabel(fileName);
		label.setHorizontalAlignment(JLabel.CENTER);
		add(label, BorderLayout.NORTH);
		BarChart model = new BarChart(list, stringX, stringY, minY, maxY, gap);
		panel.add(new BarCharComponent(model));
	}

	/**
	 * Main program where everything starts
	 * 
	 * @param args takes one argument, that is name of file
	 */
	public static void main(String[] args) {
		if (args.length != 1) {
			System.err
					.println("Invalid argument. It takes 1 argument and you gave "
							+ args.length);
			System.exit(1);
		}

		File file = new File(args[0]);
		if (!file.exists() || !file.canRead()) {
			System.err
					.println("Invalid argument. File must exist and be readable, exists = "
							+ file.exists() + ", readable = " + file.canRead());
			System.exit(1);
		}
		BufferedReader br = null;
		try {
			br = new BufferedReader(Files.newBufferedReader(file.toPath()));
			stringY = br.readLine().trim();
			stringX = br.readLine().trim();
			String[] xyvalues = br.readLine().trim().split(" ");
			for (int i = 0; i < xyvalues.length; i++) {
				int first = Integer.parseInt(xyvalues[i].substring(0,
						xyvalues[i].indexOf(',')));
				int second = Integer.parseInt(xyvalues[i].substring(
						xyvalues[i].indexOf(',') + 1, xyvalues[i].length()));
				list.add(new XYValue(first, second));
			}
			minY = Integer.parseInt(br.readLine().trim());
			maxY = Integer.parseInt(br.readLine().trim());
			gap = Integer.parseInt(br.readLine().trim());
			fileName = file.getAbsolutePath();
		} catch (IOException e) {
			System.err.println("Error while reading file");
			System.exit(1);
		} catch (Exception e) {
			System.err.println("Wrong input file");
			System.exit(1);
		} finally {
			try {
				br.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		SwingUtilities.invokeLater(() -> {
			new BarChartDemo().setVisible(true);
		});
	}
}
